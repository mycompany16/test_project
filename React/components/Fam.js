import React from 'react'


const Fam = ({onChangeParam, getTest }) => {
    return(
        <div className="container">
            <div className="row">
                <div className="col-md-7 mrgnbtm">
                <h2>Введите параметры запроса:</h2>
                <form>
                    <div className="row">
                        <div className="form-group col-md-6">
                            <label htmlFor="exampleInputEmail1">Фамилия</label>
                            <input type="text" onChange={(e) => onChangeParam(e)}  className="form-control" name="numpz" id="numpz"  />
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="exampleInputPassword1">Имя</label>
                            <input type="text" onChange={(e) => onChangeParam(e)} className="form-control" name="zavnum" id="zavnum" />
                        </div>
                    </div>                   
                    <button type="button" className="btn btn-danger" onClick= {(e) => getTest()}>Вывести данные</button>
                </form>
                </div>
            </div>
        </div>
    )
}

export default Fam