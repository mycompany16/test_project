import React from 'react'

export const Table = ({data=[]}) => {     
    
    return(
        <div className="container">
            <h2>Данные пользователей</h2>
            <h3>  </h3>
            <table className="table table-bordered">
                <thead>
                <tr>                   
                    <th>ID</th>
                    <th>email</th>
                    <th>first_name</th>
                    <th>last_name</th>
                    <th>avatar</th>
                </tr>                  
                {data.map(data =>    <tr>       
                    <th>{data.id}</th>
                    <th>{data.email}</th>
                    <th>{data.first_name}</th>
                    <th>{data.last_name}</th>
                    <th>{data.avatar}</th>
                </tr>)}
                </thead>               
            </table>           
        </div>
    )
}